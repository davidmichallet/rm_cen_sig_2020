---
size: 4:3
backgroundImage:  url(https://ariegenature.fr/wp-content/uploads/2020/11/fond.png)
---
# OPENSSH 

> SSH = Secure Shell
> 1995
> client <> serveur

## Objectifs

- Lutter contre les attaques des robots
- Sécuriser les communications entre 2 machines
- Lutter contre l'usurpation et les interceptions

---
# Démo live !

```markdown
Client : Station de travail UBUNTU 20.04
```

```markdown
Serveur : VM sous UBUNTU SERVER 18.04 LTS
```
---

# Côté Client

##  Installation du client :
```shell
sudo apt install openssh-client
```
## Générer clés publique 🗝️ / privée :key:
```
ssh-keygen -o -a 100 -t ed25519 -C "YV-ANA"
```
Options :
```markdown
> -o : nouveau format openssh au lieu de l’ancien format PEM.
> -a : nombre de tours de clé
> -t : type d'algorithme (DSA, RSA, ECDSA, **ED25519**)
> -C : commentaire pour identifier plus facilement la clé
- Passphrase pour déverrouiller la clé privée.
```
---
## Transfert notre clef "**publique**" vers le serveur
```
ssh-copy-id -i ~/.ssh/id_ed25519.pub serveur@192.168.60.9
```
> Enregistrement de la "fingerprint" (empreinte) du serveur

## Liste des fichiers .ssh du **client**

```
client@CEN:~$ ls -lah .ssh
total 20K
drwx------ 2 client client 4,0K nov.  27 10:37 .
drwxr-xr-x 5 client client 4,0K nov.  27 10:31 ..
-rw------- 1 client client  444 nov.  27 10:31 id_ed25519
-rw-r--r-- 1 client client   90 nov.  27 10:31 id_ed25519.pub
-rw-r--r-- 1 client client  222 nov.  27 10:37 known_hosts
```

---

## Mémorisons notre passphrase

- ajout la clef à l'Agent SSH
```
client@CEN ~ % ssh-add
Enter passphrase for /home/client/.ssh/id_ed25519: 
Identity added: /home/client/.ssh/id_ed25519 (DM-CEN38)
```

> :warning: Utilisation de ssh-agent pour mémoriser la passphrase durant la session. scp, sftp, rsync, ssh...


---
## Bonus : Simplifions la connection ssh

```
client@CEN ~ % cat .ssh/config 
Host bob
  HostName 192.168.60.9
  User serveur
  Port 22
  Protocol 2
```
```
ssh bob
```
---
# Configuration de OpenSSH Serveur

:warning: Attention au nom d'utilisateur trop classique (*admin, ubuntu, apache, user, postgres ...* )

## Liste des fichiers du **serveur**

```
serveur@CEN:/# ls -la /etc/ssh/
├── moduli
├── ssh_config
├── sshd_config
├── ssh_host_ecdsa_key
├── ssh_host_ecdsa_key.pub
├── ssh_host_ed25519_key
├── ssh_host_ed25519_key.pub
├── ssh_host_rsa_key
└── ssh_host_rsa_key.pub
```
---
## Modification de la config server
- Sauvegarder le fichier /etc/ssh/sshd_config
```
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.backup
sudo vim /etc/ssh/sshd_config
```

```
Minimum :
  - 1/ Refuser la connexion depuis le root
  - 2/ Accepter l’authentification par clé
  - 3/ Forcer l'utilisation du protocole SSH 2
En plus :
    - Refuser l’identification par mot de passe
    - Changer le port d'écoute de SSH
    - Identifier les utilisateurs pouvant se connecter
    - Affichage d'une bannière à la connexion
    - Augmentation du niveau de log
```
---
##  Relancer la configuration de SSH
```
sudo service sshd restart
```
> :warning: Après avoir redémarré le service sshd, garder la session ouverte et ouvrir un second terminal pour tester que tout fonctionne !

- Valider que tout fonctionne !

```
ssh serveur@192.168.60.9 -p 22
```
---
## Pour aller plus loin :

```
Possibilités :
- Rebonds : accéder à une machine (GNU/Linux) sur un réseau local depuis l'extérieur.
- sshpass : connexion SSH par mot de passe non-interactive (script bash)
- sshfs : monter un système de fichier distant à travers ssh
- Utilisation 2FA (Two-Factor Authentication)
- gitlab/gitub... : connexion automatique par clé publique
- Mise en place d'outils type **Fail2ban** : permet de Bannir une ip après x tentatives infructueuse.
...
```
### Tout est question de compromis entre sécurité & contraintes !

