---

size: 4:3
backgroundImage:  url(https://ariegenature.fr/wp-content/uploads/2020/11/fond.png)
# paginate: true
# footer: 'Footer content'

---

# Un peu de théorie

Communications sécurisées pour les nuls

![bg right width:280px](https://ariegenature.fr/wp-content/uploads/2020/11/vpn_tunnel.png)

---

# Deux objectifs

1. Chiffrer.

2. Identifier.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/deux.png)

---

# Chiffrement

Depuis l'antiquité c'est toujours la même technique : une **clé** permet à la fois de chiffrer et de déchiffrer des données. D'où le qualificatif **symétrique**.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/cryptography.png)

---

# Comment partager la clé ?

Avec des clés asymétriques pardi !

La **clé publique** sert à *chiffrer* ; la **clé privée** à *déchiffrer*.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/public_key_crypto.png)

---

# Demander sa clé publique au correspondant

![height:500px](https://ariegenature.fr/wp-content/uploads/2020/11/chiffr_envoi_cle_pub_srv.png)

---

# Chiffrer la clé symétrique

![height:500px](https://ariegenature.fr/wp-content/uploads/2020/11/chiffr_chiffr_cle_sym.png)

---

# Transmettre la clé symétrique chiffrée

![height:500px](https://ariegenature.fr/wp-content/uploads/2020/11/chiffr_envoi_cle_chiffr.png)

---

# Déchiffrer la clé symétrique

![height:500px](https://ariegenature.fr/wp-content/uploads/2020/11/chiffr_dechiffr_cle_sym.png)

---

# Enjoy !

![height:500px](https://ariegenature.fr/wp-content/uploads/2020/11/chiff_tunnel_secur.png)

---

# Identification

Comment être sûr.e que c'est bien celui ou celle que je crois qui m'envoie sa clé publique ?

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/identity.png)

---

# La méthode SSH

Lorsque le correspondant est une machine : empreinte digitale numérique (*fingerprint*).

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/openssh.png)

---

# Comment ça marche ?

1. La première fois que vous vous connectez à une machine, OpenSSH vous demande si vous faîtes confiance à cette machine (facteur humain).

2. Si l'empreinte digitale change (réinstallation par ex.), OpenSSH bloque la connexion et vous prévient que ce n'est peut-être pas la bonne machine.

---

# Message d'erreur SSH

```console
$ ssh sysad@serveur.cen.fr
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:hotsxb/qVi1/ycUU2wXF6mfGH++Yk7WYZv0r+tIhg4I.
Please contact your system administrator.
Add correct host key in /home/me/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/me/.ssh/known_hosts:47
ECDSA host key for serveur.cen.fr has changed and you have requested strict checking.
Host key verification failed.
```

---

# La méthode par certificats

On ajoute alors :
- un nouveau tiers, l'autorité, en qui on a confiance (facteur humain) ;
- des informations d'identification (nom, courriel, société, etc.).

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/certificat.png)

---

# Demander de signer sa clé publique

![height:500px](https://ariegenature.fr/wp-content/uploads/2020/11/id_csr.png)

---

# Enjoy !

![height:500px](https://ariegenature.fr/wp-content/uploads/2020/11/id_crea_cert.png)

---

# Sécurité et unicité d'une clé privée

* Une seule clé privée par utilisateur et par poste.

  - `dmichallet-ordi1-ssh.key`, `dmichallet-ordi2-ssh.key`, `dmichallet-ordi1-ssl.key`, `dmichallet-ordi2-ssl.key`

* Un seul exemplaire d'une clé privée.

* Lisible uniquement par l'utilisateur : `-r------` ou `0400`.

