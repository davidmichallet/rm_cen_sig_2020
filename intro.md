---

size: 4:3
backgroundImage:  url(https://ariegenature.fr/wp-content/uploads/2020/11/fond.png)
# paginate: true
# footer: 'Footer content'

---

# Introduction à la cryptographie asymétrique

## Applications à SSH et à PostgreSQL

Yann Voté (ANA - CEN Ariège)

David Michallet (CEN Isère)

---

# Sommaire

1. Un peu de théorie

   * Chiffrement (David)

   * Identification (Yann)

2. Clés SSH (David)

3. Connexions PostgreSQL sécurisées (Yann)
