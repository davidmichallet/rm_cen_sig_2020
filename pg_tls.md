---

size: 4:3
backgroundImage:  url(https://ariegenature.fr/wp-content/uploads/2020/11/fond.png)

---

# Connexions sécurisées avec OpenSSL, l'exemple de PostgreSQL

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/vpn_tunnel.png)

---

# La problématique

Sécuriser les connexions au service PostgreSQL sur l'adresse IP publique du serveur.

> **Note :** tout ce qui suit n'est pas utile, et même nuisible, en cas de connexion sur une adresse IP privée.

---

# L'adresse IP du client n'est pas une sécurité !

La seule adresse IP, renseignée dans le fichier `pg_hba.conf`, n'est pas suffisante : il est possible d'usurper une adresse IP.

```conf
host    ana             sig                90.76.223.26/32         md5
```

De plus, ça peut être contre-productif si l'adresse IP est attribuée dynamiquement par le fournisseur.

---

# OpenSSL

Créer une pseudo infrastructure à clés publiques (PKI)

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/logo_openssl.png)

---

# Il faut expérimenter !

Ce ne sont que des fichiers.

> **Note :** Mais une fois en production : rigueur !

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/experiment.png)

---

# Mot de passe et passphrase déconseillés

Contrairement à SSH.

Parce que pour certains logiciels, il est très compliqué de la saisir pour automatiser les connexions.

---

# Configurer le formulaire et les valeurs par défaut

/etc/ssl/openssl.cnf

```conf
[ req_distinguished_name ]
countryName			= Pays (2 lettres)
countryName_default		= FR
stateOrProvinceName		= Région
stateOrProvinceName_default	= Occitanie   # Pas d'accent
localityName			= Ville
localityName_default		= Alzen       # Pas d'accent
0.organizationName		= Société
0.organizationName_default	= CEN Ariege  # Pas d'accent
organizationalUnitName		= Unité organisationnelle
commonName			= Nom commun, CN (nom de domaine, de serveur ou d'utilisateur)
emailAddress			= Courriel
```

---

# Créer l'autorité de certificats (CA)

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/autorite.png)

---

# Créer la clé privée d'autorité

Sur l'autorité.

```console
$ openssl req -new -nodes -text -out ca-cen-ariege.csr -keyout ca-cen-ariege.key
...
Pays (2 lettres) [FR]:
Région [Occitanie]:
Ville [Alzen]:
Société [CEN Ariege]:
Unité organisationnelle:
Nom commun, CN (nom de domaine, de serveur ou d'utilisateur):ca-cen-ariege.priv.ariegenature.fr
Email Address []:ana@ariegenature.fr
...
A challenge password []:
An optional company name []:
```

Sécuriser `ca-cen-ariege.key`, jamais dupliquer.

---

# Créer le certificat d'autorité racine

Sur l'autorité.

```console
$ openssl x509 -req -in ca-cen-ariege.csr -text -days 730 -extensions v3_ca -signkey ca-cen-ariege.key -out ca-cen-ariege.pem
Signature ok
subject=C = FR, ST = Occitanie, L = Alzen, O = CEN Ariege, CN = ca-cen-ariege.priv.ariegenature.fr, emailAddress = ana@ariegenature.fr
Getting Private key
```

---

# Où placer la clé privée d'autorité ?

Déchiffrée, ni sur un serveur ouvert sur internet, ni sur un portable ou une clé USB (peuvent être volés).

Minimum : sur un serveur dans le réseau interne avec uniquement deux applis installées : OpenSSH pour l'accès et OpenSSL pour signer.

Idéalement : chiffrée (peut alors être stockée sur clé).

---

# Certificat d'un serveur

Un serveur de bases de données par exemple.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/database-server.png)

---

# Créer la clé privée pour un serveur

Sur le serveur en question (nom : db).

```console
$ openssl req -new -nodes -text -out db.csr -keyout db.key
...
Pays (2 lettres) [FR]:
Région [Occitanie]:
Ville [Alzen]:
Société [CEN Ariege]:
Unité organisationnelle []:
Nom commun, CN (nom de domaine, de serveur ou dutilisateur) []:db.ariegenature.fr
Courriel []:yann.v@ariegenature.fr
...
```

Sécuriser `db.key`, jamais dupliquer. Copier `db.csr` sur autorité pour signature.

---

# Créer le certificat du serveur

Sur l'autorité.

```console
$ openssl x509 -req -in db.csr -text -days 730 -CA ca-cen-ariege.pem -CAkey ca-cen-ariege.key -CAcreateserial -out db.pem
Signature ok
subject=C = FR, ST = Occitanie, L = Alzen, O = CEN Ariege, CN = db.priv.ariegenature.fr, emailAddress = yann.v@ariegenature.fr
Getting CA Private Key
```

Copier `db.pem` et `ca-cen-ariege.pem` sur db.

---

# Certificat d'un utilisateur

Un utilisateur qui souhaite se connecter au serveur par exemple.

> **Rappel :** une clé privée par utilisateur *et* par poste.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/ThinMappingCLient.png)

---

# Créer la clé privée pour un utilisateur

Sur le poste de l'utilisateur.

```console
$ openssl req -new -nodes -text -out dmichallet.csr -keyout dmichallet.key
...
Pays (2 lettres) [FR]:
Région [Auvergne-Rhone-Alpes]:
Ville [Saint-Egreve]:
Société [CEN Isere]:
Unité organisationnelle []:
Nom commun, CN (nom de domaine, de serveur ou dutilisateur) []:dmichallet
Courriel []:david.michallet@cen-isere.org
...
```

Sécuriser `dmichallet.key`, jamais dupliquer.  Envoyer `dmichallet.csr` au propriétaire du serveur pour signature.

---

# Signer le certificat d'un utilisateur

Sur l'autorité.

```console
$ openssl x509 -req -in dmichallet.csr -text -days 730 -CA ca-cen-ariege.pem -CAkey ca-cen-ariege.key -CAcreateserial -out dmichallet-cen-ariege.pem
Signature ok
subject=C = FR, ST = Auvergne-Rhone-Alpes, L = Saint-Egreve, O = CEN Isere, CN = dmichallet, emailAddress = david.michallet@cen-isere.org
Getting CA Private Key
```

Envoyer à l'utilisateur `dmichallet-cen-ariege.pem` et `ca-cen-ariege.pem`.

---

# Certificat d'une machine cliente

Un serveur qui souhaite se connecter à la base de données par exemple, pour faire du FDW.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/terminal-server.png)

---

# Créer la clé privée pour une machine

Sur le serveur (nom : srv-isere).

```console
$ openssl req -new -nodes -text -out srv-isere.csr -keyout srv-isere.key
...
Pays (2 lettres) [FR]:
Région [Auvergne-Rhone-Alpes]:
Ville [Saint-Egreve]:
Société [CEN Isere]:
Unité organisationnelle []:
Nom commun, CN (nom de domaine, de serveur ou dutilisateur) []:srv-isere
Courriel []:david.michallet@cen-isere.org
...
```

Sécuriser `srv-isere.key`, jamais dupliquer.  Envoyer `srv-isere.csr` au propriétaire du serveur pour signature.

---

# Signer le certificat du serveur « client »

Sur l'autorité.

```console
$ openssl x509 -req -in srv-isere.csr -text -days 730 -CA ca-cen-ariege.pem -CAkey ca-cen-ariege.key -CAcreateserial -out srv-isere-cen-ariege.pem
Signature ok
subject=C = FR, ST = Auvergne-Rhone-Alpes, L = Saint-Egreve, O = CEN Isere, CN = srv-isere, emailAddress = david.michallet@cen-isere.org
Getting CA Private Key
```

Envoyer à l'utilisateur `srv-isere-cen-ariege.pem` et `ca-cen-ariege.pem`.

---

# Configurer PostgreSQL pour SSL

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/postgres.png)

---

# Activer SSL sur le serveur Postgres

Dans `postgresql.conf`.

```conf
ssl = true
ssl_cert_file = '/etc/ssl/certs/db.pem'
ssl_key_file = '/etc/ssl/private/db.key'
ssl_ca_file = '/etc/ssl/certs/ca-cen-ariege.pem'
```

---

# Autoriser un certificat

Pour chaque certificat à autoriser, il faut créer un utilisateur PG *avec le même CN*.

```sql
CREATE ROLE dmichallet WITH ...
```

Puis, dans `pg_hba.conf`.

```conf
hostssl   ana             dmichallet           all             md5
hostssl   ana             srv-isere            all             md5
```

---

# Côté client

Il faut indiquer :
- le chemin de la clé privée ;
- le chemin du certificat client ;
- le chemin du certificat autorité ;
- l'option `verify-full`.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/ThinMappingCLient.png)

---

# psql

```console
$ psql "host=db.ariegenature.fr user=dmichallet dbname=ana \
> sslrootcert=/etc/ssl/certs/ca-cen-ariege.pem \
> sslcert=/home/dmichallet/.ssl/dmichallet-cen-ariege.pem \
> sslkey=/home/dmichallet/.ssl/dmichallet.key \
> sslmode=verify-full"
```

---

# pgAdmin

![bg right width:400px](https://ariegenature.fr/wp-content/uploads/2020/11/pgadmin_ssl.png)

---

# Client PostgreSQL et FDW

```sql
CREATE SERVER cen_ariege_db
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host 'db.ariegenature.fr', port '5432', dbname 'ana',
         sslrootcert '/etc/ssl/certs/ca-cen-ariege.pem',
         sslcert '/var/lib/postgresql/.ssl/srv-isere-cen-ariege.pem',
         sslkey '/var/lib/postgresql/.ssl/srv-isere.key',
         sslmode 'verify-full');
```

---

# QGIS 3 : ajout certificat et clé privée perso

![bg right width:400px](https://ariegenature.fr/wp-content/uploads/2020/11/qgis_import_cert.png)

---

# QGIS 3 : ajout certificat autorité

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/qgis_import_ca.png)

---

# QGIS 3 : ajout du compte PostgreSQL

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/qgis_add_auth_config.png)

---

# QGIS 3 : ajout de la connexion PostgreSQL

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/qgis_add_pg_conn.png)


---

# Faire attention au délai d'expiration

Pas trop court pour que ce soit pas trop chiant, mais pas trop long pour que ça ait un sens.

2 ans me semble être un bon compromis.

Se mettre des rappels !

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/Remember-Remixed.png)

---

# Penser aux listes de révocations (CRL)

Si une clé privée est compromise, révoquer le certificat associé !

Envoyer le fichier .crl au serveur db.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/PanneauInterdictionBase.png)

---

# Bientôt une doc détaillée

Qui regroupera les certificats, les bonnes pratiques, le cycle de vie, les liste de révocation, etc.

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/Red-Coming-Soon-Stamp.png)
