---

size: 4:3
backgroundImage:  url(https://ariegenature.fr/wp-content/uploads/2020/11/fond.png)
# paginate: true
# footer: 'Footer content'

---

# Merci de votre attention !

Des questions ?

![bg right width:300px](https://ariegenature.fr/wp-content/uploads/2020/11/Thank-you-pinned-note.png)

---

# Crédits

La plupart des illustrations viennent de [Openclipart](https://openclipart.org/).

Le logo OpenSSH vient du [site web](https://www.openssh.com/).

Les diagrammes de la section théorie ont été réalisés avec [draw.io](https://drawio-app.com/) et sa bibliothèque d'illustrations.
