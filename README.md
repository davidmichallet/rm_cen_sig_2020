<div style="text-align:center"><img src="https://reseau-cen.org/sites/all/themes/tssks/img/website-logo.png" title="Fédération des CEN" width="200" height="73" /></div>
    
Codes sources des présentations TLS et SSH des rencontres métiers géomatique 2020 des conservatoires d'espaces naturels
=======================================================================================================================

Ces rencontres ont eu lieu le lundi 30 novembre 2020 en visioconférence.

Présentation à deux voix :

- Yann Voté (ANA - CEN Ariège) pour la partie TLS & OpenSSL ;
- David Michallet (CEN Isère) pour la partie SSH.

Le code source des présentations suit la syntaxe
[Markdown](https://fr.wikipedia.org/wiki/Markdown) avec les contraintes
[Marp](https://marp.app/) pour la rédaction des diapositives.


<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Open_Source_Initiative_keyhole.svg/220px-Open_Source_Initiative_keyhole.svg.png" title="Open Source" width="50" height="50" />  Outils Open Source
